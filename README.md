# Ubuntu22p04 Preparation

## Install Basic Application
```
sudo apt update && sudo apt dist-upgrade && sudo apt install openssh-server build-essential git nvme-cli samba python-is-python3 fio
```

## Add Swap For Ubuntu

[Reference 1](https://linuxize.com/post/how-to-add-swap-space-on-ubuntu-20-04/)
[Reference 2](https://wiki.archlinux.org/title/swap)

1. Create swapfile
    ```bash
    sudo swapoff /swapfile
    sudo fallocate -l 16G /swapfile
    sudo chmod 600 /swapfile
    sudo mkswap /swapfile
    sudo swapon /swapfile
    ```

2. Make the SWAP file mount permanently (2 method)

    - Modify the /etc/fstab through NANO
        ```bash
        sudo nano /etc/fstab
        ```
        Add this line "/swapfile none swap sw 0 0" in the end of /etc/fstab
        ```bash
        # /etc/fstab: static file system information.
        #
        # Use 'blkid' to print the universally unique identifier for a
        # device; this may be used with UUID= as a more robust way to name devices
        # that works even if disks are added and removed. See fstab(5).
        #
        # <file system> <mount point>   <type>  <options>       <dump>  <pass>
        # / was on /dev/sda2 during installation
        UUID=a400d8d0-2361-49a4-95bd-8db20b7ec411 /               ext4    errors=remount-ro 0       1
        # /boot/efi was on /dev/sda1 during installation
        UUID=53CD-4226  /boot/efi       vfat    umask=0077      0       1
        # ======== Add "/swapfile none swap sw 0 0" below ========
        /swapfile       none            swap    sw              0       0

        ```

    - Use below command to modify /etc/fstab.

        *Must check if this line is existed or not, Ubuntu 22.04 default has this line.*

        ```bash
        echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
        ```


## Make Ubuntu 22.04 screen share password permanent

[Original askUbuntu Post][Url_Original askUbuntu Post]

This issue is caused by the default keyring not being unlocked when the VNC server starts. Since you are using "automatic login", no password is ever entered on reboot and the keyring remains locked.

As seen in this related [answer][Url_answer], there is a reasonably secure way to complete this setup that will leave the RDP password unencrypted while preserving encryption on your other passwords. Some solutions suggest using no login password, but this is very insecure as it would store all your passwords in plain text (unencrypted).

1. Open "Passwords and Keys" app on the desktop.

    ![]() <img src="img/ubuntu_22.04_screen_share_password_permanent_01.png"  width="600">

2. Create a new "Password Keyring" using the "+" icon.

    ![]() <img src="img/ubuntu_22.04_screen_share_password_permanent_02.png"  width="600">

3. Name the new keyring "Zero Security Keyring" or something that reminds you it will be un-encrypted. Leave the password blank so that the keychain is unencrypted. You will be warned that you are creating an unencrypted keychain.

    ![]() <img src="img/ubuntu_22.04_screen_share_password_permanent_03.png"  width="600">

4. Right-click on the new keyring and choose "set as default"

    ![]() <img src="img/ubuntu_22.04_screen_share_password_permanent_04.png"  width="600">

5. Click on the old "Default" keyring and delete "GNOME Remote Desktop RDP Credentials"

    ![]() <img src="img/ubuntu_22.04_screen_share_password_permanent_05.png"  width="600">

6. Open settings and set a new RDP password

    ![]() <img src="img/ubuntu_22.04_screen_share_password_permanent_06.png"  width="600">

7. Check that the password was stored under the "Zero Security Keyring"

    ![]() <img src="img/ubuntu_22.04_screen_share_password_permanent_07.png"  width="600">

8. Right click on "Default" keyring and choose "set as default"

    ![]() <img src="img/ubuntu_22.04_screen_share_password_permanent_08.png"  width="600">

[Url_Original askUbuntu Post]: https://askubuntu.com/questions/1396745/21-10-make-screen-share-password-permanent
[Url_answer]: https://askubuntu.com/a/1387556/154524

## Setup SAMBA share folder

*Example: The SAMBA share folder is located at "/home/user/sambashare".*

1. Install SAMBA and enable it
    ```bash
    sudo apt-get install samba
    # sudo systemctl enable --now smbd
    ```

2. Make SAMBA share folder
    ```bash
    mkdir /home/user/sambashare
    sudo chmod -R ugo+w /home/user/sambashare
    # sudo chmod -R 775 /home/user/sambashare
    ```

3. Config SAMBA server.
    ```bash
    sudo nano /etc/samba/smb.conf
    ```

    Add the following lines in the end. "username" should replace according to your envirement

    ```bash
    [sambashare]
    comment = Samba on Ubuntu
    path = /home/user/sambashare
    create mode = 0644
    force create mode = 0644
    directory mode = 0755
    force directory mode = 0755
    read only = no
    browsable = yes
    ```

    or

    ```bash
    [sambashare]
    comment = Samba on Ubuntu
    path = /home/user/sambashare
    read only = no
    browsable = yes
    ```

4. Add an user for SAMBA server with independent password.
    ```bash
    sudo smbpasswd -a "username"
    ```

5. Restart service and add firewall rule
    ```bash
    sudo service smbd restart
    # sudo systemctl restart smbd
    ```

## Fix "caps lock" not working through VNC but normal in phy and RDP.

1. Install gnome-tweaks and run it.
    ```
    sudo apt install gnome-tweaks
    ```

2. Using "gnome-tweaks" to fix the issues.

    ![]() <img src="img/capslock01.png"  width="660">

    ![]() <img src="img/capslock02.png"  width="660">









